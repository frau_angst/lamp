#ifndef BASECOMMAND_H
#define BASECOMMAND_H

#include "lampwidget.h"

#include <QDataStream>

#define REGISTER_TYPE(T, COMMAND_CODE) \
static int commandTypeId = qRegisterMetaType<T>(qPrintable(QString::number(COMMAND_CODE)));

class BaseCommand
{
public:
    BaseCommand();
    virtual ~BaseCommand() {}

    virtual void execute(LampWidget *) = 0;
    virtual quint64 needBytes() const = 0;
    virtual void setValue(QDataStream &in) = 0;

private:
    LampWidget *lamp;
signals:

public slots:

};

#endif // BASECOMMAND_H

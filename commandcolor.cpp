#include "commandcolor.h"

REGISTER_TYPE(CommandColor, 0x20)

CommandColor::CommandColor() :
    color(Qt::red)
{
}

void CommandColor::execute(LampWidget *lamp)
{
    lamp->setColor(color);
}

void CommandColor::setValue(QDataStream &in)
{
    quint32 rgb = 0;
    in.readRawData(reinterpret_cast<char *>(&rgb), needBytes());

    color.setRgb(rgb);
}

#ifndef COMMANDCOLOR_H
#define COMMANDCOLOR_H

#include "basecommand.h"

#include <QColor>

class CommandColor : public BaseCommand
{
public:
    CommandColor();

    void execute(LampWidget *lamp);
    quint64 needBytes() const {return 3;}
    void setValue(QDataStream &in);

private:
    QColor color;
};

Q_DECLARE_METATYPE(CommandColor)

#endif // COMMANDCOLOR_H

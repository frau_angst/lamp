#include "commandoff.h"

REGISTER_TYPE(CommandOff, 0x13)

void CommandOff::execute(LampWidget *lamp)
{
    lamp->off();
}

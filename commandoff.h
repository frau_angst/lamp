#ifndef COMMANDOFF_H
#define COMMANDOFF_H

#include "basecommand.h"

class CommandOff : public BaseCommand
{
public:
    void execute(LampWidget * lamp);
    quint64 needBytes() const {return 0;}
    void setValue(QDataStream &) {}

};

Q_DECLARE_METATYPE(CommandOff)

#endif // COMMANDOFF_H

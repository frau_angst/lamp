#include "commandon.h"

REGISTER_TYPE(CommandOn, 0x12)

void CommandOn::execute(LampWidget *lamp)
{
    lamp->on();
}

#ifndef COMMANDON_H
#define COMMANDON_H

#include "basecommand.h"

class CommandOn : public BaseCommand
{
public:
    void execute(LampWidget * lamp);
    quint64 needBytes() const {return 0;}
    void setValue(QDataStream &) {}

};

Q_DECLARE_METATYPE(CommandOn)

#endif // COMMANDON_H

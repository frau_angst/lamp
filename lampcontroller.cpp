#include "lampcontroller.h"
#include "lampmanager.h"
#include "mainwindow.h"

#include <QTcpSocket>


LampController::LampController(LampWidget *lamp, MainWindow *mainWindow) :
    QObject(mainWindow)
{
    ioDevice = new QTcpSocket(this);
    new LampManager(ioDevice, lamp, this);

    connect(mainWindow, SIGNAL(connectToHost(QString,int)), this, SLOT(onConnectToHost(QString,int)));
    connect(ioDevice, SIGNAL(connected()), mainWindow, SLOT(onConnected()));
}

void LampController::onConnectToHost(const QString &host, int port)
{
    ioDevice->disconnectFromHost();
    ioDevice->connectToHost(host, port);
}

#ifndef LAMPCONTROLLER_H
#define LAMPCONTROLLER_H

#include <QObject>

class QTcpSocket;
class LampWidget;
class MainWindow;

class LampController : public QObject
{
    Q_OBJECT
public:
    LampController(LampWidget * lamp, MainWindow *mainWindow);

signals:

public slots:
    void onConnectToHost(const QString &host, int port);

private:
    QTcpSocket * ioDevice;
};

#endif // LAMPCONTROLLER_H

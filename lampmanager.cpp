#include "lampmanager.h"

#include "basecommand.h"

#include <QDataStream>
#include <QMetaType>

LampManager::LampManager(QIODevice * input, LampWidget *lampWidget, QObject *parent) :
    QObject(parent),
    ioDevice(input),
    lamp(lampWidget),
    needReadHeader(true)
{
    connect(ioDevice, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void LampManager::onReadyRead()
{
    const qint64 headerSize = sizeof(commandType) + sizeof(length);
    qint64 bytesAvailable = ioDevice->bytesAvailable();
    QDataStream in(ioDevice);
    in.setByteOrder(QDataStream::BigEndian);

    while (bytesAvailable >= headerSize) {
        if (needReadHeader) {
            in >> commandType >> length;
            bytesAvailable -= headerSize;
            needReadHeader = false;
        }
        if (bytesAvailable < length) {
            break;
        }

        QString commandName = QString::number(commandType);
        int commandId = QMetaType::type(qPrintable(commandName));
        if (commandId != QMetaType::UnknownType) {
            void * commandPtr = QMetaType::create(commandId);
            BaseCommand * command = static_cast<BaseCommand *>(commandPtr);
            if (length == command->needBytes()) {
                command->setValue(in);
                command->execute(lamp);
                length = bytesAvailable - ioDevice->bytesAvailable();
            }
            QMetaType::destroy(commandId, commandPtr);
        }
        skipBytes(in);
        bytesAvailable -= length;
    }
}

void LampManager::skipBytes(QDataStream & in)
{
    in.skipRawData(length);
    needReadHeader = true;
}

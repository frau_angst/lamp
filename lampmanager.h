#ifndef LAMPMANAGER_H
#define LAMPMANAGER_H

#include <QObject>
#include <QIODevice>

class LampWidget;

class LampManager : public QObject
{
    Q_OBJECT
public:
    explicit LampManager(QIODevice *input, LampWidget *lampWidget, QObject *parent = 0);

signals:
    void lighterCommand();

public slots:
    void onReadyRead();

private:
    QIODevice * ioDevice;
    LampWidget * lamp;
    bool needReadHeader;
    quint8 commandType;
    quint16 length;

    void readData();
    void skipBytes(QDataStream &in);
};

#endif // LAMPMANAGER_H

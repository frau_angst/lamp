#include "lampwidget.h"

#include <QPainter>
#include <QTimerEvent>

LampWidget::LampWidget(QWidget *parent) :
    QWidget(parent),
    color(Qt::green),
    lampOn(false),
    timerId(0)
{
    color.setAlpha(0);
}

void LampWidget::on()
{
    lampOn = true;
    if(timerId == 0)
        timerId = startTimer(20);
}

void LampWidget::off()
{
    lampOn = false;
    if(timerId == 0)
        timerId = startTimer(20);
}

void LampWidget::setColor(const QColor &lampColor)
{
    color.setRgb(lampColor.red(), lampColor.green(), lampColor.blue(), color.alpha());
    repaint();
}

void LampWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen pen(Qt::black);
    pen.setWidth(20);
    painter.setPen(pen);
    painter.setBrush(color);
    painter.drawRoundedRect(30, 30, width()-60, height()-60, 20, 20);
}

void LampWidget::timerEvent(QTimerEvent *event)
{
    if (lampOn && color.alpha() != 255) {
        color.setAlpha(color.alpha() + 5);
    }
    else if (!lampOn && color.alpha() != 0){
        color.setAlpha(color.alpha() - 5);
    }
    else{
        killTimer(event->timerId());
        timerId = 0;
    }
    repaint();
}

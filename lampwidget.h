#ifndef LAMPWIDGET_H
#define LAMPWIDGET_H

#include <QWidget>

class LampWidget : public QWidget
{
    Q_OBJECT
public:
    LampWidget(QWidget *parent = 0);

    void on();
    void off();
    void setColor(const QColor &lampColor);

private:
    QColor color;
    bool lampOn;
    int timerId;

protected:
    void paintEvent(QPaintEvent *);
    void timerEvent(QTimerEvent *event);

};

#endif // LAMPWIDGET_H

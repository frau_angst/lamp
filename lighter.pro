#-------------------------------------------------
#
# Project created by QtCreator 2015-02-07T22:44:42
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lighter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    basecommand.cpp \
    commandon.cpp \
    commandoff.cpp \
    commandcolor.cpp \
    lampwidget.cpp \
    lampmanager.cpp \
    lampcontroller.cpp

HEADERS  += mainwindow.h \
    basecommand.h \
    commandon.h \
    commandoff.h \
    commandcolor.h \
    lampwidget.h \
    lampmanager.h \
    lampcontroller.h

FORMS    += mainwindow.ui

#include "lampcontroller.h"
#include "mainwindow.h"
#include "lampwidget.h"

#include <QApplication>
#include <QTcpSocket>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LampWidget * lamp = new LampWidget;
    MainWindow w(lamp);
    w.show();
    LampController controller(lamp, &w);


    return a.exec();
}

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lampwidget.h"

#include <QDialog>
#include <QGridLayout>
#include <QUrl>

MainWindow::MainWindow(LampWidget * lamp, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QLayout *layout = new QGridLayout(ui->lampFrame);
    layout->addWidget(lamp);

    connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(onConnectButtonClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onConnectButtonClicked()
{
    QUrl url("//" + ui->address->text(), QUrl::StrictMode);
    url.setScheme("");
    if (url.isValid()) {
        qDebug("host %s, port %i", qPrintable(url.host()), url.port());
        emit connectToHost(url.host(), url.port());
    }
}

void MainWindow::onConnected()
{
    ui->connectButton->setDisabled(true);
}

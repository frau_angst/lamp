#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class LampWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(LampWidget *lamp, QWidget *parent = 0);
    ~MainWindow();

signals:
    void connectToHost(const QString &host, int port);
public slots:
    void onConnectButtonClicked();
    void onConnected();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
